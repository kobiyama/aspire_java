package web.form;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class LoginForm {
    @Size(min=8)
    private String id;

    @Size(min=8)
    @Pattern(regexp="[0-9a-zA-z]+")
    private String pass;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getPass() {
        return pass;
    }
    public void setPass(String pass) {
        this.pass = pass;
    }
}
