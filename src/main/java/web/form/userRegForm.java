/*
 * userRegForm
 * ユーザ登録のフォームクラス
 *
 * @author M.Gontani
 *
 * @version 1.0
 *
 */
package web.form;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import org.hibernate.validator.constraints.Email;
import javax.validation.constraints.NotNull;

public class userRegForm {
    @Size(min=8)
    private String newId;

    @Size(min=8)
    @Pattern(regexp="[0-9a-zA-z]+")
    private String newPass;

    @NotNull
    @Size(min = 1, max = 50)
    @Email
    private String mailAddress;

    public String getNewId() {
        return newId;
    }
    public void setNewId(String newId) {
        this.newId = newId;
    }
    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(String mailAddress) {
        this.mailAddress = mailAddress;
    }
}
