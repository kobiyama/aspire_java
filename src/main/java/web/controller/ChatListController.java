package web.controller;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import web.form.LoginForm;



@Controller
public class ChatListController {
    private static final Logger log = LoggerFactory
.getLogger(ChatListController.class);

    @RequestMapping("/chatList")
    public String postValidSample(@ModelAttribute("form") @Valid LoginForm form, BindingResult result, Model model) {

        if (result.hasErrors()) {
            for(FieldError err: result.getFieldErrors()) {
                log.debug("error code = [" + err.getCode() + "]");
            }
        }
        return "chatList";
    }
}