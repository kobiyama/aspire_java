package web.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;



@Controller
public class DemoController {

  @RequestMapping("/test")
  public String home(Model model) {
	  List<String> list = Arrays.asList("ユーザ情報","チャット","招待");
      model.addAttribute("beans", list);
      return "test";
  }
}